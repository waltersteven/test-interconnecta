//var BASE_URL = "https://crm.zoho.com/crm/private/json/Leads/getRecords?";
var BASE_URL = "https://crm.zoho.com/crm/private/json/Leads/getRecords?newFormat=1&authtoken=4f86a4c7e8f1b2505ff9ba5ae58f3c53&scope=crmapi&fromIndex=1&toIndex=100&selectColumns=leads(First Name,Last Name,Email)&version=1";


/*! jQuery v3.3.1 | (c) JS Foundation and other contributors | jquery.org/license */
/*! jQuery v3.3.1 | (c) JS Foundation and other contributors | jquery.org/license */

var settings = {};
/*$(document).ready( function(){


	fetch(BASE_URL, settings)
	.then(response => response.json())
	.then(json => loadData(json))

});*/

$( "#fetchDataButton" ).click(function() {

	console.log("hey");
	fetch(BASE_URL, settings)
	.then(response => response.json())
	.then(json => loadData(json.response.result.Leads.row))
});


function loadData(obj) {
    var leads = "";



    obj.forEach(element => {
	    var firstname = "";
		var lastname = "";
    	var email = "";
        var id = element.no;
        var usr = element.FL;

        if(usr[1].content != null){
        	 firstname = usr[1].content;
        }
        if(usr[2].content != null){
        	 lastname = usr[2].content;
        }
        if(usr[3] != null){
        	 email = usr[3].content;

        }

        var row = `<tr>
      				<th scope="row">${id}</th>
     				<td>${firstname}</td>
      				<td>${lastname}</td>
      				<td>${email}</td>
    				</tr>`;

        console.log(firstname);	
        leads += row;
    });


    //console.log(leads);
    $("#tableBody").empty();
    $("#tableBody").append(leads);
}
